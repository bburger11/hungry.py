#!/usr/bin/env python3

import pprint
import sys
import os
import requests
from requests_html import HTMLSession

def usage(status=0):
    print('''Usage: {} today/tomorrow
    
    -f  MEAL    hungry (-f)or which MEAL?
                (breakfast, lunch, latelunch, dinner)
                (default: all)
    -a  DH      (-a)t which DINING HALL
                (ndh, sdh)
                (defaul: ndh)

    ex: hungry -f dinner -a ndh today
            '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)



def load_menu(url, MEALS, today_flag, tomorrow_flag):
    session = HTMLSession()
    r = session.get(URL)
    r.html.render()
    menu_data = r.html.text
    menu_data = menu_data.splitlines()
    menu_data = menu_data[31:-49]
    for index, item in enumerate(menu_data):
        if "Allergens" in item:
            del menu_data[index]

    for index, item in enumerate(menu_data):
        if not index:
            continue
        if "Menu" in item:
            today = menu_data[:index-1]
            tomorrow = menu_data[index:]
            break
   
    if today_flag == True:
        today = list_to_dictionary(today)
        print_menu(today, MEALS)
    elif tomorrow_flag == True:
        tomorrow = list_to_dictionary(tomorrow)
        print_menu(tomorrow, MEALS)

    return None

def list_to_dictionary(data):
    organized_menu = {}
    MEAL_WORDS = {"Breakfast (", "Lunch (", "Dinner ("}  
    for index, item in enumerate(data):
        if "Menu" in item:
            organized_menu[item] = {}
            day = item
            del data[index]
        if any(x in item for x in MEAL_WORDS):
            organized_menu[day][item] = []
            del data[index]
            data.insert(index, 100)

    # Really dumb cheese strats cause its busted:
    temp_dict = {}
    temp_dict.update({data[0]: []})
    organized_menu[day] = {**temp_dict,**organized_menu[day]}
    del data[0]
    
    for day, dictionary in organized_menu.items():
        for meal in dictionary:
            for index, item in enumerate(data):
                if item == 100:
                    data = data[index+1:]
                    break
                else:
                    organized_menu[day][meal].append(item)
                      
    return organized_menu

def print_menu(data, MEALS):
    for key, value in data.items():
        chars = len(key)
        print(key)
    for i in range(chars):
        print('=',end='')
    print("")    
    
    for day, meals_list in data.items():
        for meal, food_items in meals_list.items():
            if any(x in meal for x in MEALS):
                print("{:>30}".format(meal))
                for food in food_items:
                    print("\t\t\t{}".format(food))
    
    return None

# main

MEALS = ["Breakfast (", "Lunch (", "Late Lunch (", "Dinner ("]
URL = "https://dining.nd.edu/locations-menus/north-dining-hall/"
args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-h':
        usage(0)
    elif arg == '-f':
        if args[0] == "breakfast":
            MEALS = ["Breakfast ("]
        elif args[0] == "lunch":
            MEALS = ["Lunch ("]
        elif args[0] == "latelunch":
            MEALS = ["Late Lunch ("]
        elif args[0] == "dinner":
            MEALS = ["Dinner ("]
        else:
            usage(1)
        args.pop(0)
    elif arg == '-a':
        if args[0] == 'ndh':
            URL = "https://dining.nd.edu/locations-menus/north-dining-hall/"
        elif args[0] == 'sdh':
            URL = "https://dining.nd.edu/locations-menus/south-dining-hall/" 
        else:
            usage(1)
        args.pop(0)
    else:
        usage(1)


if len(args) != 1:
    usage(1)
else:
    day_choice = args[0]

today_flag = False
tomorrow_flag = False
if day_choice == "today":
    today_flag = True
elif day_choice == "tomorrow":
    tomorrow_flag = True
else:
    usage(1)

load_menu(URL, MEALS, today_flag, tomorrow_flag)


