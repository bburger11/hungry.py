## Picture This

You live on Notre Dame's campus, you have a meal plan, it's like 3:00pm, 
and you're getting a little peckish. 
It's sorta almost time to go to the dining hall to get dinner but you 
have to wait around for 4:30 at the earliest. Suddenly you think:

>  What if the dining hall only has Malibu vegan gardenburgers, steamed quinoa, 
and those chicken patties that tasted good like once

You need to go to the dining hall's website and decide if you're gonna 
have to blow another $7.59 on two quesaritos from Taco Bell. But before you 
pull out that *bulky* web browser, you imagine:

> What if there was some way I could check the dining hall menus, right from my terminal?

Sweet buttery biscuits do I have the python script for you.

## A Mouth Watering Script

Hungry.py is a simple python script to fetch data for the meals being served at North and 
South dining halls for breakfast, lunch, late lunch, and dinner, and it can give information 
about the current day or the following day.

Hungry.py is built on [requests-html](https://github.com/kennethreitz/requests-html). To install 
this library:

`pip3 install --user requests-html`

Finally, to run the program, refer to the usage guide ('hungry.py -h brings up usage')

```
Usage: hungry today/tomorrow
    -f  MEAL    hungry (-f)or which MEAL?
                (breakfast, lunch, latelunch, dinner)
                (default: all)
    -a  DH      (-a)t which DINING HALL
                (ndh, sdh)
                (defaul: ndh)
    ex: hungry -f dinner -a ndh today
```

The script itself was a bit of a hack job, so if it breaks, oh well. Feel free to add to 
it as you see fit.

Also, make sure you always specify "today" or "tomorrow" 
whenever you run the program, classic mistake to make (bad design?).

Bon Appétit